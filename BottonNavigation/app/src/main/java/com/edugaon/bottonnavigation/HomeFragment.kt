package com.edugaon.bottonnavigation

import android.content.res.Configuration
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView

class HomeFragment : Fragment() {

    private lateinit var drawerToggle: ActionBarDrawerToggle
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val homeNav = view.findViewById<NavigationView>(R.id.homeNav_view)
        val navHeaderView = homeNav.inflateHeaderView(R.layout.home_nava_header_layout)
        val accountTextView = view.findViewById<TextView>(R.id.accounts_homeNaveDrawer_textView)

        val drawerImage = view.findViewById<ImageView>(R.id.drawer_imageView)
        val drawerLayout = view.findViewById<DrawerLayout>(R.id.drawerLayout_homePage)
        drawerImage.setOnClickListener {
            drawerLayout.openDrawer(Gravity.LEFT)
        }

        drawerToggle =
            ActionBarDrawerToggle(activity, drawerLayout, R.string.open, R.string.close)

        drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        activity?.actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true
        }
        return true
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerToggle.onConfigurationChanged(newConfig)
    }

}
